﻿namespace Snap
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Reset = new System.Windows.Forms.Button();
            this.playerBack = new System.Windows.Forms.PictureBox();
            this.cpuBack = new System.Windows.Forms.PictureBox();
            this.playerPlayed = new System.Windows.Forms.PictureBox();
            this.cpuPlayed = new System.Windows.Forms.PictureBox();
            this.playerScoreboard = new System.Windows.Forms.Label();
            this.cpuScoreboard = new System.Windows.Forms.Label();
            this.signGameStatus = new System.Windows.Forms.Label();
            this.gameStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.playerBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerPlayed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuPlayed)).BeginInit();
            this.SuspendLayout();
            // 
            // Reset
            // 
            this.Reset.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Reset.Location = new System.Drawing.Point(293, 291);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(75, 23);
            this.Reset.TabIndex = 0;
            this.Reset.Text = "Reset";
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.button1_Click);
            this.Reset.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPressed);
            // 
            // playerBack
            // 
            this.playerBack.BackgroundImage = global::Snap.Properties.Resources.BackOfCard;
            this.playerBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.playerBack.Location = new System.Drawing.Point(12, 214);
            this.playerBack.Name = "playerBack";
            this.playerBack.Size = new System.Drawing.Size(75, 100);
            this.playerBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.playerBack.TabIndex = 1;
            this.playerBack.TabStop = false;
            // 
            // cpuBack
            // 
            this.cpuBack.BackgroundImage = global::Snap.Properties.Resources.BackOfCard;
            this.cpuBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cpuBack.Location = new System.Drawing.Point(302, 12);
            this.cpuBack.Name = "cpuBack";
            this.cpuBack.Size = new System.Drawing.Size(75, 100);
            this.cpuBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cpuBack.TabIndex = 3;
            this.cpuBack.TabStop = false;
            // 
            // playerPlayed
            // 
            this.playerPlayed.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.playerPlayed.Location = new System.Drawing.Point(93, 214);
            this.playerPlayed.Name = "playerPlayed";
            this.playerPlayed.Size = new System.Drawing.Size(75, 100);
            this.playerPlayed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.playerPlayed.TabIndex = 4;
            this.playerPlayed.TabStop = false;
            // 
            // cpuPlayed
            // 
            this.cpuPlayed.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cpuPlayed.Location = new System.Drawing.Point(221, 12);
            this.cpuPlayed.Name = "cpuPlayed";
            this.cpuPlayed.Size = new System.Drawing.Size(75, 100);
            this.cpuPlayed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cpuPlayed.TabIndex = 5;
            this.cpuPlayed.TabStop = false;
            // 
            // playerScoreboard
            // 
            this.playerScoreboard.AutoSize = true;
            this.playerScoreboard.Location = new System.Drawing.Point(48, 157);
            this.playerScoreboard.Name = "playerScoreboard";
            this.playerScoreboard.Size = new System.Drawing.Size(82, 13);
            this.playerScoreboard.TabIndex = 6;
            this.playerScoreboard.Text = "Player Score : 0";
            // 
            // cpuScoreboard
            // 
            this.cpuScoreboard.AutoSize = true;
            this.cpuScoreboard.Location = new System.Drawing.Point(271, 157);
            this.cpuScoreboard.Name = "cpuScoreboard";
            this.cpuScoreboard.Size = new System.Drawing.Size(75, 13);
            this.cpuScoreboard.TabIndex = 7;
            this.cpuScoreboard.Text = "CPU Score : 0";
            // 
            // signGameStatus
            // 
            this.signGameStatus.AutoSize = true;
            this.signGameStatus.Location = new System.Drawing.Point(165, 157);
            this.signGameStatus.Name = "signGameStatus";
            this.signGameStatus.Size = new System.Drawing.Size(68, 13);
            this.signGameStatus.TabIndex = 8;
            this.signGameStatus.Text = "Game Status";
            // 
            // gameStatus
            // 
            this.gameStatus.AutoSize = true;
            this.gameStatus.Location = new System.Drawing.Point(177, 183);
            this.gameStatus.Name = "gameStatus";
            this.gameStatus.Size = new System.Drawing.Size(35, 13);
            this.gameStatus.TabIndex = 9;
            this.gameStatus.Text = "label2";
            this.gameStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 326);
            this.Controls.Add(this.gameStatus);
            this.Controls.Add(this.signGameStatus);
            this.Controls.Add(this.cpuScoreboard);
            this.Controls.Add(this.playerScoreboard);
            this.Controls.Add(this.cpuPlayed);
            this.Controls.Add(this.playerPlayed);
            this.Controls.Add(this.cpuBack);
            this.Controls.Add(this.playerBack);
            this.Controls.Add(this.Reset);
            this.Name = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.playerBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerPlayed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuPlayed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.PictureBox playerBack;
        private System.Windows.Forms.PictureBox cpuBack;
        private System.Windows.Forms.PictureBox playerPlayed;
        private System.Windows.Forms.PictureBox cpuPlayed;
        private System.Windows.Forms.Label playerScoreboard;
        private System.Windows.Forms.Label cpuScoreboard;
        private System.Windows.Forms.Label signGameStatus;
        private System.Windows.Forms.Label gameStatus;
    }
}

