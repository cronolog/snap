﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Diagnostics;
using System.Timers;



namespace Snap
{
    public partial class Form1 : Form
    {
        public List<int> playerDeck = new List<int>();
        public List<int> playerPlayDeck = new List<int>();
        public List<int> cpuDeck = new List<int>();
        public List<int> cpuPlayDeck = new List<int>();
        public int[] cardIndex = new int[52];
        public Image[] globalCards = new Image[52];
        public Random rng = new Random();
        public int playerCard, cpuCard;
        public int playerScore, cpuScore;
        public System.Timers.Timer aiDelay = new System.Timers.Timer();
        public Stopwatch stopWatch = new Stopwatch();
        public bool turn = true;


        public Form1()
        {
            //loads images into the values corresponding to globalCards
            for (int i = 0; i < 52;)            {
                cardIndex[i] = i;
                globalCards[i] = Image.FromFile("Cards/(" + ++i + ").JPG");//cheese
            }
            Shuffle();
            InitializeComponent();
            aiDelay.Interval = 500f;
        }

        private void button1_Click(object sender, EventArgs e)
        {
 
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F)
            {
                MessageBox.Show("What the Ctrl+F?");
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        //i wonder what this does
        void Shuffle()
        {
            playerDeck.Clear();
            playerPlayDeck.Clear();
            
            cpuDeck.Clear();
            cpuPlayDeck.Clear();

            for (int i = 0; i < 52; i++)//shuffles ze deck 
            {
                int randomCard = rng.Next(52);
                int indexedCard = cardIndex[i];
                cardIndex[i] = cardIndex[randomCard];
                cardIndex[randomCard] = indexedCard;
            }
            
            //splits into playerDeck and cpuDeck
            for (int i = 0; i < 26; i++)
            {
                playerDeck.Add(cardIndex[i]);
            }
            for (int i = 26; i < 52; i++)
            {
                cpuDeck.Add(cardIndex[i]);
            }

            for (int i = 0; i < 26; i++)
            { 
                //testcode- writes values to console
                Console.Write(playerDeck[i]);
                Console.Write(" ");
                Console.WriteLine(cpuDeck[i]);
            }

        }
        //flip function for determining what happens when flip occurs
        void flip(bool flippingPlayer)
        {
            List<int> focusDeck;
            PictureBox flipping;
            if (flippingPlayer)
            {
                focusDeck = playerDeck;
                flipping = playerPlayed;
                //shuffles if no cards 
                if (focusDeck.Count == 0)
                {
                    Shuffle();
                }
                //takes away the played card from the deck
                playerCard = playerDeck[focusDeck.Count - 1];
                //adds that card to the played deck
                playerPlayDeck.Add(playerCard);
                //test statement
            
            }
            else
            {
                focusDeck = cpuDeck;
                flipping = cpuPlayed;
                if (focusDeck.Count == 0)
                {
                    Shuffle();
                }
                cpuCard = cpuDeck[focusDeck.Count - 1];
                cpuPlayDeck.Add(cpuCard);
    
            }
           
            flipping.Image = globalCards[focusDeck[focusDeck.Count-1]];
            focusDeck.RemoveAt(focusDeck.Count - 1);

            if (playerCard % 13 == cpuCard % 13)
            {
                stopWatch.Start();
            }
        }

        private void KeyPressed(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'a' && turn)
            {
                flip(true);
                gameStatus.Visible = false;
                if(playerDeck.Count == 0)
                {
                    gameStatus.Visible = true;
                    gameStatus.Text = "Player 1 wins!";
                    Shuffle();
                }

                EnemyAi();
                turn = !turn;

               
            }


            if (e.KeyChar == 's' && turn)
            {
                snap(true);
                gameStatus.Visible = false;
                if (playerCard % 13 == cpuCard % 13)
                {
                    updateAi();
                }
            }

        

        }

        void snap(bool snappingPlayer)
        {
            if(playerCard%13 == cpuCard % 13)
            {
                if (snappingPlayer)
                {

                    playerScoreboard.Text += "help";
                    Console.Write(cpuScore);
                    playerScoreboard.Text = "Player Score : " + ++playerScore;
                    gameStatus.Text = "You win!";
                    gameStatus.Visible = true;
                    //throws played cards at the cpu
                    cpuDeck.AddRange(playerPlayDeck);
                    cpuDeck.AddRange(cpuPlayDeck);
                    cpuPlayDeck.Clear();
                    playerPlayDeck.Clear();
                    //test statement
                    Console.WriteLine("cpu has " + cpuDeck.Count + " cards");
                }
                else
                {
                    cpuScoreboard.Text += "help";
                    Console.Write(cpuScore); 
                    cpuScoreboard.Text = "Cpu Score : " + ++cpuScore;
                    gameStatus.Text = "Too slow! You lose!";
                    gameStatus.Visible = true;
                    //throws played cards at the player
                    playerDeck.AddRange(playerPlayDeck);
                    playerDeck.AddRange(cpuPlayDeck);
                    cpuPlayDeck.Clear();
                    playerPlayDeck.Clear();
                    Console.WriteLine("you has " + playerDeck.Count + " cards");
                }
            }
            else
            {
                if (snappingPlayer)
                {
                    cpuScoreboard.Text = "Cpu Score : " + ++cpuScore;
                    gameStatus.Text = "You snapped wrong!";
                    gameStatus.Visible = true;
                    //throws played cards at the player
                    playerDeck.AddRange(playerPlayDeck);
                    playerDeck.AddRange(cpuPlayDeck);
                    cpuPlayDeck.Clear();
                    playerPlayDeck.Clear();
                    Console.WriteLine("player has " + playerDeck.Count + " cards");
                }
                else
                {
                    playerScoreboard.Text = "Player Score : " + ++playerScore;
                    gameStatus.Text = "They snapped wrong!";
                    gameStatus.Visible = true;
                    //throws played cards at the cpu
                    cpuDeck.AddRange(playerPlayDeck);
                    cpuDeck.AddRange(cpuPlayDeck);
                    cpuPlayDeck.Clear();
                    playerPlayDeck.Clear();
                    Console.WriteLine("cpu has " + cpuDeck.Count + " cards");
                }

            }
            
        }

        double playerDelay;
 
        void updateAi()
        {
            //stops the time
            stopWatch.Stop();
            //made playerdelay = to the stopwatch recorded time
            playerDelay = stopWatch.Elapsed.TotalMilliseconds;
            //set stopwatch time to the interval for the timer
            aiDelay.Interval = Convert.ToInt32(playerDelay);
            //link timer to the ai play function
            aiDelay.Elapsed += AiPlays;
        }
               
        void EnemyAi()
        {
            flip(false);
            turn = !turn;

            if (playerCard % 13 == cpuCard % 13)
            {
                //run the fun
                aiDelay.Enabled = true;
            }    
            
        }

        public void AiPlays(Object source, System.Timers.ElapsedEventArgs e)
        {
            snap(false);
        }

    }
}
